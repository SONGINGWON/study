object Main {
    /* Expressions
    println(1)
    println(1 + 1)
    println("Hello!")
    println("Hello," + " world!") */

    /* Values
    val x = 1 + 1
    println(x) 
    val x : Int = 1 + 1*/
    
    /* Variables
    var x = 1 + 1
    x = 3
    println(x * x) 
    var x : Int = 1 + 1 */
    
    /* Blocks
    println({
        val x = 1 + 1
        x + 1
    }) */

    /* Functions 
    (x: Int) => x + 1 

    val addOne = (x: Int) => x + 1
    println(addOne(1))
    val add = (x: Int, y: Int) => x + y
    println(add(1, 2))
    val getTheAnswer = () => 42
    println(getTheAnswer()) */

    /* Methods
    def add(x: Int, y:Int): Int = x + y
    println(add(1, 2))

    def addThenMultiply(x: Int, y: Int)(multiplier: Int): Int = (x + y) * multiplier
    println(addThenMultiply(1, 2)(3))

    def name: String = System.getProperty("user.name")
    println("Hello, " + name + "!")

    def getSquareString(input: Double): String = {
        val square = input * input
        square.toString
    }
    println(getSquareString(2.5)) */

    /* Classes
    class Greater(prefix: String, suffix: String){
        def greet(name: String): Unit = {
            println(prefix + name + suffix)
        }
    }

    val greater = new Greater("Hello, ", "!")
    greater.greet("Scala developer") */

    /* Case Classes
    case class Point(x: Int, y: Int)
    val point = Point(1, 2)
    val anotherPoint = Point(1, 2)
    val yetAnotherPoint = Point(2, 2)

    if(point == anotherPoint){
        println(point + " and " + anotherPoint + " are the same.")
    }else{
        println(point + " and " + anotherPoint + " are different.")
    }

    if(point == yetAnotherPoint){
        println(point + " and " + yetAnotherPoint + " are the same.")
    }else{
        println(point + " and " + yetAnotherPoint + " are different.")
    } */

    /* Objects
    object IdFactory{
        private var counter = 0
        def create(): Int = {
            counter += 1
            counter
        }
    }

    val newId: Int = IdFactory.create()
    println(newId)
    val newerId: Int = IdFactory.create()
    println(newerId) */

    /* Traits
    trait Greater{
        def greet(name: String): Unit = 
            println("Hello, " + name + "!")
    }

    class DefaultGreater extends Greater

    class CustomizableGreater(prefix: String, postfix: String) extends Greater{
        override def greet(name: String): Unit = {
            println(prefix + name + postfix)
        }
    }

    val greeter = new DefaultGreater()
    greeter.greet("Scala developer")

    val customerGreeter = new CustomizableGreater("How are you, ", "?")
    customerGreeter.greet("Scala developer") */

    def main(args: Array[String]): Unit = 
        println("Hello, Scala developer!")

}