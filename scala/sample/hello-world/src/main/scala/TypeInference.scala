object Main extends App{
    val businessName = "Montreux Jazz Café"

    def squareOf(x: Int) = x * x
    def fac(n: Int) = if (n == 0){1} else {n * fac(n - 1)}

    case class MyPair[A, B](x: A, y: B)
    val p = MyPair(1, "scala")

    def id[T](x: T) = x
    val q = id(1)

    Seq(1, 3, 4).map(x => x * 2)
    var obj = null
    obj = new AnyRef
}

