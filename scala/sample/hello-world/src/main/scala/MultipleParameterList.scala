object Main{
    def main(args: Array[String]): Unit = {
        example3()
    }

    def example1(): Unit = {
        val numbers = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        //val res = numbers.foldLeft(0)((m, n) => m + n)
        val res = numbers.foldLeft(0)(_ + _)
        println(res);
    }

    //이건 모르겠다
    /* def execute(arg: Int)(implicit ec: scala.concurrent.ExecutionContext) = ???
    def example2(): Unit = {
        val numbers = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        println(execute(0)(numbers))
    } */

    //PARTIAL APPLICATION 모르겠음
    def example3(): Unit = {
        val numbers = List(1,2,3,4,5,6,7,8,9,10)
        val numberFunc = numbers.foldLeft(List[Int]()) _

        val squares = numberFunc((xs, x) => xs :+ x*x)
        println(squares)

        val cubes = numberFunc((xs, x) => xs :+ x*x*x)
        println(cubes)
    }

}
