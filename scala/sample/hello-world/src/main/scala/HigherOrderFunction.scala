object Main {
    def main(args: Array[String]): Unit = {
        example7()
    }

    def example1(): Unit = {
        val salaries = Seq(20000, 70000, 40000)
        val doubleSalary = (x: Int) => x * 2
        val newSalaries = salaries.map(doubleSalary)

        newSalaries.foreach(e => println(e))
    }

    def example2(): Unit = {
        val salaries = Seq(20000, 70000, 40000)
        val newSalaries = salaries.map(x => x * 2)

        newSalaries.foreach(e => println(e))
    }

    def example3(): Unit = {
        val salaries = Seq(20000, 70000, 40000)
        val newSalaries = salaries.map(_ * 2)

        newSalaries.foreach(e => println(e))
    }

    def example4(): Unit = {
        val FORECAST = new WeeklyWeatherForecast(
            Seq(8.8, 10.2, 29.3))
        println(FORECAST.forecastInFahrenheit)
    }

    def example5(): Unit = {
        println(SalaryRaiser.smallPromotion(
            List(10.1, 20.3, 30.4)))
        println(SalaryRaiser.greatPromotion(
            List(10.1, 20.3, 30.4)))
        println(SalaryRaiser.hugePromotion(
            List(10.1, 20.3, 30.4)))
    }

    def example6(): Unit = {
        println(SalaryRaiser2.smallPromotion(
            List(10.1, 20.3, 30.4)))
        println(SalaryRaiser2.greatPromotion(
            List(10.1, 20.3, 30.4)))
        println(SalaryRaiser2.hugePromotion(
            List(10.1, 20.3, 30.4)))
    }

    def example7(): Unit = {
        def urlBuilder(ssl: Boolean, domainName: String): (String, String) => String = {
            val schema = if(ssl){
                "https://"
            }else{
                "http://"
            }

            (endpoint: String, query: String) => s"$schema$domainName/$endpoint?$query"
        }

        val domainName = "www.example.com"
        def getUrl = urlBuilder(ssl=true, domainName)
        val endpoint = "users"
        val query = "id=1"
        val url = getUrl(endpoint, query)
        
        println(url)
    }

}

case class WeeklyWeatherForecast(temperatures: Seq[Double]){
    private def convertCtoF(temp: Double) = temp * 1.8 + 32
    def forecastInFahrenheit: Seq[Double] = temperatures.map(convertCtoF)
}

object SalaryRaiser{
    def smallPromotion(salaries: List[Double]): List[Double] = 
        salaries.map(salary => salary * 1.1)
    def greatPromotion(salaries: List[Double]): List[Double] = 
        salaries.map(salary => salary * math.log(salary))
    def hugePromotion(salaries: List[Double]): List[Double] = 
        salaries.map(salary => salary * salary)
}

object SalaryRaiser2 {
    private def promotion(salaries: List[Double], promotionFunction: Double => Double): List[Double] = 
        salaries.map(promotionFunction)
    def smallPromotion(salaries: List[Double]): List[Double] = 
        promotion(salaries, salaries => salaries * 1.1)
    def greatPromotion(salaries: List[Double]): List[Double] = 
        promotion(salaries, salary => salary * math.log(salary))
    def hugePromotion(salaries: List[Double]): List[Double] = 
        promotion(salaries, salary => salary * salary)
}