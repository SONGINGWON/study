object Main {

    def main(args: Array[String]): Unit = {
/*         val user1 = new User

        val point1 = new Point(2, 3)
        println(point1.x)
        println(point1)

        val origin = new Point2 // x and y are both set to 0
        val point2 = new Point2(1)
        println(point2.x)

        val point2_1 = new Point2(2)
        println(point2_1.y)
 */ 
        /* val point3 = new Point3
        point3.x = 99
        point3.y = 101 // prints the warning */

        /* val point4 = new Point4(1,2)
        point4.x = 3 // error: reassignment to val */

        val point5 = new Point5(1,2)
        println(point5.x) // error: value x is not a member of Point5
    }

}

class User

class Point(var x: Int, var y: Int){

    def move(dx: Int, dy: Int): Unit = {
        x = x + dx
        y = y + dy
    }

    override def toString: String = 
        s"($x, $y)"
}

class Point2(var x: Int = 0, var y: Int = 0)

class Point3 {
    private var _x = 0
    private var _y = 0
    private val bound = 100

    def x = _x
    def x_= (newValue: Int): Unit = {
        if (newValue < bound) {
            _x = newValue 
        }else {
            printWarning
        }
    }

    def y = _y
    def y_= (newValue: Int): Unit = {
        if(newValue < bound){
            _y = newValue
        }else{
            printWarning
        }
    }

    private def printWarning = println("WARNING: Out of bounds")

}

class Point4(val x: Int, val y: Int)
class Point5(x: Int, y: Int)