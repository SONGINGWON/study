import scala.util.Random

object CustomerID {
    def apply(name: String) = s"$name--${Random.nextLong}"

    def unapply(customerID: String): Option[String] = {
        val stringArray: Array[String] = customerID.split("--")
        if(stringArray.tail.nonEmpty){
            Some(stringArray.head)
        }else{
            None
        }
    }
}

object Main{
    def main(args: Array[String]): Unit = {
        example1()
    }
    def example1(): Unit = {
        val customer1ID = CustomerID("Roy")
        customer1ID match {
            case CustomerID(name) => println(name)
            case _ => println("Could not extract a CustomerID")
        }

        val customer2ID = CustomerID("Nico")
        val CustomerID(name) = customer2ID
        println(name)

        val CustomerID(name2) = "--asdfasdfasdf"
        val CustomerID(name3) = "-asdfasdfasdf"
    }
}