import scala.collection.mutable.ArrayBuffer

trait HairColor

trait Iterator[A] {
    def hasNext: Boolean
    def next(): A
}

class IntIterator(to: Int) extends Iterator[Int] {
    private var current = 0
    override def hasNext: Boolean = current < to
    override def next(): Int = {
        if(hasNext) {
            val t = current
            current += 1
            t
        }else{
            0
        }
    }
}

object Main{
    def main(args: Array[String]): Unit = {
       //usingTraits() 
       subTyping()
    }

    def usingTraits(): Unit = {
        val iterator = new IntIterator(10)
        println(iterator.next()) // returns 0
        println(iterator.next()) // returns 1
    }

    def subTyping(): Unit = {
        val dog = new Dog("Harry")
        val cat = new Cat("Sally")

        val animals = ArrayBuffer.empty[Pet]
        animals.append(dog)
        animals.append(cat)
        animals.foreach(pet => println(pet.name)) // Prints Harry Sally
    }
}

trait Pet {
    val name: String
}

class Cat(val name: String) extends Pet
class Dog(val name: String) extends Pet
