
import scala.annotation.tailrec

object DeprecationDemo extends App {
    @deprecated("deprecation message", "release # which deprecates method")
    def hello = "hola"

    hello

    println(Annotations.factorial(10))
}

object Annotations {
    def factorial(x: Int): Int = {
        @tailrec
        def factorialHelper(x: Int): Int = {
            if(x==1) {
                1
            } else {
                factorialHelper(x - 1)
            }
        }

        factorialHelper(x)
    }
}

@Source(URL = "https://coders.com/", 
        mail = "support@coders.com")
class MyClass extends TheirClass{

}

@SourceURL("https://coders.com")
class MyScalaClass {

}