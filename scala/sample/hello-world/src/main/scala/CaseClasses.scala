object Main{
    def main(args: Array[String]): Unit = {
        example3()
    }

    def example1(): Unit = {
        val frankenstein = Book("978-0486282114")
    }

    def example2(): Unit = {
        val message1 = Message("guillaume@quebec.ca", "jorge@catalonia.es", "Ça va ?")
        val message2 = Message("guillaume@quebec.ca", "jorge@catalonia.es", "Ça va ?")
        val messagesAreTheSame = message1 == message2
        println(messagesAreTheSame)
    }

    def example3(): Unit = {
        val message4 = Message("julien@bretagne.fr", "travis@washington.us", "Me zo o komz gant ma amezeg")
        val message5 = message4.copy(sender = message4.recipient, recipient = "claire@bourgogne.fr")
        println(message5.sender)
        println(message5.recipient)
        println(message5.body)
    }
}

case class Book(isbn: String)
case class Message(sender: String, recipient: String, body: String)
