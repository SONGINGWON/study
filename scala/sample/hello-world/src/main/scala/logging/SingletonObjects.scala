/* import logging.Logger.info

class Project(name: String, daysToComplete: Int)

class Test {
    val project1 = new Project("TPS Reports", 1)
    val project2 = new Project("Website redesign", 5)
    info("Created projects")
} */
import scala.math._

case class Circle(radius: Double){
    import Circle._
    def area: Double = calculateArea(radius)
}

object Circle {
    private def calculateArea(radius: Double): Double = Pi * pow(radius, 2.0)
}

object Main {
    def main(args: Array[String]): Unit = {
        
    }
    def example1(): Unit = {
        val circle1 = Circle(5.0)

        println(circle1.area)
    }
    def example2(): Unit = {
        val scalaCenterEmail = Email.fromString("scala.center@epfl.ch")
        scalaCenterEmail match {
            case Some(email) => println(
                s"""Registered an email
                    |Username: ${email.username}
                    |Domain name: ${email.domainName}
                """.stripMargin)
            case None => println("Error: could not parse email")
        }
    }
}

class Email(val username: String, val domainName: String)

object Email {
    def fromString(emailString: String): Option[Email] = {
        emailString.split("@") match {
            case Array(a, b) => Some(new Email(a,b))
            case _ => None
        }
    }
}