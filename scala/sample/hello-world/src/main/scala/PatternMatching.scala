import scala.util.Random

object Main {
    def main(args: Array[String]): Unit = {
        example3()
    }

    def example1(): Unit = {
        val x: Int = Random.nextInt(10)

        x match {
            case 0 => println("zero")
            case 1 => println("one")
            case 2 => println("two")
            case _ => println("other")
        }

        def matchTest(x: Int): String = x match {
            case 1 => "one"
            case 2 => "two"
            case _ => "other"
        }
        println("########")
        println(matchTest(3))
        println(matchTest(1))
    }

    def showNotification(notification: Notification): String = {
        notification match {
            case Email(sender, title, _) =>
                s"You got an email from $sender with title: $title"
            case SMS(number, message) => 
                s"You got an SMS from $number! Message: $message"
            case VoiceRecording(name, link) =>
                s"You received a Voice Recording from $name! Click the link to hear it: $link"
        }
    }

    def example2(): Unit = {
        val someSms = SMS("12345", "Are you there?")
        val someVoiceRecording = VoiceRecording("Tom", "voicerecording.org/id/123")

        println(showNotification(someSms))
        println(showNotification(someVoiceRecording))
    }

    def showImportantNotification(notification: Notification, importantPeopleInfo: Seq[String]): String = {
        notification match {
            case Email(sender, _, _) if importantPeopleInfo.contains(sender) => 
                "You got an email from special someone!"
            case SMS(number, _) if importantPeopleInfo.contains(number) =>
                "You got an SMS from special someone!"
            case other =>
                showNotification(other)
        }
    }

    def example3(): Unit = {
        val importantPeopleInfo = Seq("867-5309", "jenny@gmail.com")
        
        val someSms = SMS("123-4567", "Are you there?")
        val someVoiceRecording = VoiceRecording("Tom", "voicerecording.org/id/123")
        val importantEmail = Email("jenny@gmail.com", "Drinks tonight?", "I'm free after 5!")
        val importantSms = SMS("867-5309", "I'm here! Where are you?")

        println(showImportantNotification(someSms, importantPeopleInfo))
        println(showImportantNotification(someVoiceRecording, importantPeopleInfo))
        println(showImportantNotification(importantEmail, importantPeopleInfo))
        println(showImportantNotification(importantSms, importantPeopleInfo))
    }

    def goIdle(device: Device) = device match {
        case p: Phone => p.screenOff
        case c: Computer => c.screenSaverOn
    }

    def findPlaceToSit(piece: Furniture): String = piece match {
        case a: Couch => "Lie on the couch"
        case b: Chair => "Sit on the chair"
    }
}

abstract class Notification
case class Email(sender: String, title: String, body: String) extends Notification
case class SMS(caller: String, message: String) extends Notification
case class VoiceRecording(contactName: String, link: String) extends Notification

abstract class Device
case class Phone(model: String) extends Device {
    def screenOff = "Turning screen off"
}
case class Computer(model: String) extends Device {
    def screenSaverOn = "Turning screen saver on..."
}

sealed abstract class Furniture
case class Couch() extends Furniture
case class Chair() extends Furniture
