trait User {
    def username: String
}

trait Tweeter {
    this: User => 
    def tweet(tweetText: String) = println(s"$username: $tweetText")
}

class VerifiedTweeter(val username_ : String) extends Tweeter with User{
    def username = s"real $username_"
}

object Main extends App{
    val realBeyonce = new VerifiedTweeter("Beyonce")
    realBeyonce.tweet("Just spilled my glass of lemonade")
}