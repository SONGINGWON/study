class Stack[A] {
    private var elements: List[A] = Nil
    def push(x: A) {elements = x :: elements}
    def peek: A = elements.head
    def pop(): A = {
        val currentTop = peek
        elements = elements.tail
        currentTop
    }
}

object Main{
    def main(args: Array[String]): Unit = {
        example2()
    }

    def example1(): Unit = {
        val stack = new Stack[Int]
        stack.push(1)
        stack.push(2)
        println(stack.pop)
        println(stack.pop)
    }

    def example2(): Unit = {
        val stack = new Stack[Fruit]
        val apple = new Apple
        val banana = new Banana

        stack.push(apple)
        stack.push(banana)
        println(stack.pop)
        println(stack.pop)
    }
}

class Fruit
class Apple extends Fruit
class Banana extends Fruit