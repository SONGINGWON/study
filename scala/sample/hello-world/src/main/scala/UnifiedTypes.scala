object Main {

    def main(args: Array[String]): Unit = {
        //scalaTypeHierarchy()
        typeCasting()
    } 

    def scalaTypeHierarchy(): Unit = {
        val list: List[Any] = List(
            "a string", 
            732,
            'c',
            true,
            () => "an anonymous function returning a string"
        )

        list.foreach(element => println(element))
    }

    def typeCasting(): Unit = {
        val x: Long = 987654321
        val y: Float = x //9.87654321 (note that some precision is lost in this case)

        val face: Char = '☺'
        val number: Int = face //9786

        println(x)
        println(y)
        println(face)
        println(number)

        val k: Long = 987654321
        val z: Float = k
        val kk: Long = z //Does not conform
    }

}